package org.covid19.initiative.db;

public interface TimeSeriesDBConnection<T> {
    void save(T t);

    /**
     * Closes the DB connection
     */
    void close();
}
