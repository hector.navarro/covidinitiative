package org.covid19.initiative.db;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.InfluxDBClientOptions;

public class InfluxDBConnection {
    private final InfluxDBClient influxDB;

    public InfluxDBClient getInfluxDB() {
        return influxDB;
    }

    public InfluxDBConnection() {
        // TODO: Get this data from properties file
        this("http://localhost:9999", "organizacion", "bucket");
    }

    public InfluxDBConnection(
            String url,
            String org,
            String bucket
    ) {
        String token = "token from influxdb";
        InfluxDBClientOptions options = InfluxDBClientOptions.builder()
                .url(url)
                .authenticateToken(token.toCharArray())
                .org(org)
                .bucket(bucket)
                .build();
        influxDB = InfluxDBClientFactory.create(options);
    }
}
