package org.covid19.initiative.messages.serialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Serializer;
import org.covid19.initiative.model.PatientMeasure;

public class PatientMeasureSerializer implements Serializer<PatientMeasure> {
    @Override
    public byte[] serialize(String topic, PatientMeasure data) {
        byte[] serializedBytes = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            serializedBytes = objectMapper.writeValueAsString(data).getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return serializedBytes;
    }
}
