package org.covid19.initiative.messages;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.covid19.initiative.model.PatientMeasure;
import java.util.Properties;

public class MessageProducer {
    private static MessageProducer instance;
    private Properties properties;

    private MessageProducer() {
        this.properties = new Properties();
        this.properties.put("bootstrap.servers", "127.0.0.1:9092");
        this.properties.put("acks", "all");
        this.properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        this.properties.put("value.serializer", "org.covid19.initiative.messages.serialization.PatientMeasureSerializer");
    }

    public static MessageProducer getInstance() {
        if (instance == null) {
            instance = new MessageProducer();
        }
        return instance;
    }

    public void sendMessage(PatientMeasure measure) {
        Producer<String, PatientMeasure> producer = new KafkaProducer<String, PatientMeasure>(this.properties);
        producer.send(new ProducerRecord<String, PatientMeasure>("test", measure.getPatientCode(), measure));
        producer.close();
    }
}
