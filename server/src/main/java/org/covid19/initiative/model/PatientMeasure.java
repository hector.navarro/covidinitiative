package org.covid19.initiative.model;

import java.io.Serializable;

public class PatientMeasure implements Serializable {
    private String patientCode;
    private Double temperature;

    public String getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(String patientCode) {
        this.patientCode = patientCode;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "patientCode: " + patientCode + "; temperature: " + temperature;
    }
}
