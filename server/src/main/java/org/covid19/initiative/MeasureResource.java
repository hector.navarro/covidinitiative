package org.covid19.initiative;

import com.influxdb.client.WriteApi;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;
import org.covid19.initiative.db.InfluxDBConnection;
import org.covid19.initiative.messages.MessageProducer;
import org.covid19.initiative.model.PatientMeasure;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.time.Instant;

@Path("/measure")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MeasureResource {
    @POST
    public void add(PatientMeasure measurement) {
        System.out.println(measurement.toString());

        InfluxDBConnection conn = new InfluxDBConnection();
        try (WriteApi writeApi = conn.getInfluxDB().getWriteApi()) {

            //
            // Write by Data Point
            //
            Point point = Point.measurement("patient")
                    .time(Instant.now().toEpochMilli(), WritePrecision.MS)
                    .addField("temp", measurement.getTemperature())
                    .addField("status", "good")
                    .addTag("patientCode", measurement.getPatientCode());

            writeApi.writePoint(point);
        }
        MessageProducer.getInstance().sendMessage(measurement);
        conn.getInfluxDB().close();
    }

}
