package org.covid19.initiative.service.common;

public interface ICommonService<T> {
    public void save(T t);
}
