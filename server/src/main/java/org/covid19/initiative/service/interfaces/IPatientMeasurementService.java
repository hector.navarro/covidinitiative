package org.covid19.initiative.service.interfaces;

import org.covid19.initiative.model.PatientMeasure;
import org.covid19.initiative.service.common.ICommonService;

public interface IPatientMeasurementService extends ICommonService<PatientMeasure> {
}
