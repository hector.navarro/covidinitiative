package org.covid19.initiative;

import io.quarkus.runtime.StartupEvent;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

@ApplicationScoped
public class AppLifeCycleBean {
    void onStart(@Observes StartupEvent ev) {

    }
}
