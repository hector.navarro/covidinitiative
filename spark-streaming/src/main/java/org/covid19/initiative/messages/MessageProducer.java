package org.covid19.initiative.messages;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

public class MessageProducer {
    private static MessageProducer instance;
    private Properties properties;

    private MessageProducer() {
        this.properties = new Properties();
        this.properties.put("bootstrap.servers", "127.0.0.1:9092");
        this.properties.put("acks", "all");
        this.properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        this.properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    }

    public static MessageProducer getInstance() {
        if (instance == null) {
            instance = new MessageProducer();
        }
        return instance;
    }

    public void sendMessage(String msg) {
        Producer<String, String> producer = new KafkaProducer<String, String>(this.properties);
        producer.send(new ProducerRecord<String, String>("test-result", "msg-result", msg));
        producer.close();
    }
}
